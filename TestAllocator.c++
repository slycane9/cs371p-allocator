// ----------------------------------------
// projects/c++/allocator/TestAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<char,    40>,
    my_allocator<int, 400>>
    my_types_1;


TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_construct) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, check_contents) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}


// --------------
// TestAllocator
// --------------

TEST(TestAllocator3, fill_heap) {
    my_allocator<char, 100> x;
    x.allocate(91);
    ASSERT_EQ(x[0], -92);
}


TEST(TestAllocator4, check_alloc) {
    my_allocator<int, 100> x;
    int*    p = x.allocate(4);
    int v = 4;
    int s = 8;
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(x[0], -16);
    }
}



TEST(TestAllocator5, fill_heap_with_ints) {
    my_allocator<int, 16> x;
    x.allocate(1);
    ASSERT_EQ(x[0], -8);
}


TEST(TestAllocator6, fill_heap_with_doubles) {
    my_allocator<double, 16> x;
    x.allocate(1);
    ASSERT_EQ(x[0], -8);
}


TEST(TestAllocator7, fill_heap_with_ints2) {
    my_allocator<int, 100> x;
    x.allocate(21);
    ASSERT_EQ(x[0], -92);
}

TEST(TestAllocator8, fill_heap_with_doubles2) {
    my_allocator<double, 100> x;
    x.allocate(10);
    ASSERT_EQ(x[0], -92);
}

TEST(TestAllocator9, constant_construct) {
    my_allocator<char, 500> x;
    ASSERT_EQ(x[0], 492);
}


TEST(TestAllocator10, constant_construct2) {
    my_allocator<char, 10> x;
    ASSERT_EQ(x[0], 2);
}



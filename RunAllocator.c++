// ---------------------------------------
// projects/c++/allocator/RunAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ---------------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <fstream>  // ifstream
#include <iostream> // cout, endl, getline
#include <string>   // s


// --------
// includes
// --------

#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

#include "Allocator.h"

using namespace std;




// ----
// main
// ----

void alloc_solve(istream &r, ostream &w) {

    string s;
    getline(r,s);

    const int tests = stoi(s);

    getline(r,s);

    for( int test = 0; test < tests; test++) {

        my_allocator<double, 1000> a;

        //cout << "test" << test << endl;

        while(getline(r,s) && !s.empty()) {

            int val = stoi(s);

            //cout << val << endl;

            if(val > 0)
                a.allocate(val);
            else {

                val = abs(val);
                char* p = (char*)&a[0];

                int* s = (int*)p;

                while (val != 0) {

                    s = (int*)p;

                    if (*s < 0)
                        --val;

                    if (val != 0)
                        p = p + 8 + abs(*s);

                }

                a.deallocate((double*)(p + 4), 8);

            }


        }

        int* walk = &a[0];
        int* sent = nullptr;

        while (walk <= &a[999]) {
            sent = (int*)walk;
            w << *sent << " ";
            walk = (int*)((char*)walk + 8 + abs(*sent));

        }
        w << endl;

    }



}




int main () {
    using namespace std;
    alloc_solve(cin, cout);

    return 0;
}

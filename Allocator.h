// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * returns false if sentinels don't match in size or if there are 2 free blocks in a row
     */
    bool valid () const {

        int* walk = (int*)a;
        int* sent = walk;

        while (((char*)walk) < a + N) {
            sent = walk;
            walk = (int*)((char*)walk + 4 + abs(*sent));


            //check if sentinels have correct values
            if (*sent != *walk)
                return false;



            int* next = (int*)((char*)walk + 4);

            //check contiguous free nodes
            if (((char*)next) < a + N && *walk > 0 && *next > 0 )
                return false;

            walk = next;

        }


        /*int walk = 0;
        int sent = (*this)[0];
        int temp = 0;

        while (walk < N) {

        	sent = (*this)[walk];

            walk = (walk + 4 + abs(sent));

            temp = (*this)[walk];

            //check if sentinels have correct values
            if (sent != temp)
                return false;

            int next = walk + 4;

            //check contiguous free nodes
            if ((*this)[walk] > 0 && next < N && (*this)[next] > 0 )
                return false;

            walk = next;

        }*/



        return true;
    }


public:

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {

        if( N < sizeof(T) + 8)
            throw std::bad_alloc();


        int* head = (int*)a;
        int* foot = (int*)(a + N - 4);

        *head = N - 8;
        *foot = N - 8;

        //(*this)[0] = N - 2*sizeof(int);
        //(*this)[N-sizeof(int)] = N - 2*sizeof(int);

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type s) {

        int size = s * sizeof(T);



        if(size > N - 8)
            throw std::bad_alloc();


        char* head = a;

        char* result = nullptr;


        while (head < a + N && result == nullptr) {

            int* sent = (int*)head;
            int original = *sent;

            //found a fit
            if(*sent >= size) {

                //must leave an allocable block
                if((*sent - size) < ((8) + sizeof(T)))
                    size = *sent;

                //mark header as filled
                *sent = size * -1;


                //traverse to footer
                sent = (int*)(head + 4 + abs(*sent));

                //mark footer as filled
                *sent = size * -1;


                //mark the rest of the block as free
                if (original != size) {

                    sent = (int*)((char*)sent + 4);


                    *sent = original - size - 8;

                    sent = (int*)(head + 4 + original);

                    *sent = original - size - 8;



                }

                result = head + 4;

            }

            //doesn't fit, goto next node
            else
                head = head + 8 + abs(*sent);


        }

        assert(valid());

        if (result == nullptr)
            throw std::bad_alloc();

        return reinterpret_cast<pointer>(result);

    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * frees the block with its first element at the provided pointer
     * looks to the left and right to see if free blocks need to merge
     */
    void deallocate (pointer head, size_type size) {

        if( ((char*)head) < a || ((char*)head) > a + N)
            throw std::invalid_argument("p is outside of array range");


        char* me = (char*)((char*)head - 4);

        int* mySize = (int*)me;

        //free my node header for size calculations
        *mySize = *mySize * -1;

        //grab the sentinels next to us
        int* leftSize = (int*)(me - 4);
        int* rightSize = (int*)(me + 8 + *mySize);


        bool leftGood =  leftSize >= (int*)a && *leftSize > 0;
        bool rightGood = rightSize < (int*)(a+N) && *rightSize > 0;

        //merge both sides
        if(leftGood && rightGood) {

            int* newNode = (int*)((char*)leftSize - *leftSize - 4);

            int newSize = *mySize + *leftSize + *rightSize + 16;

            int* newNodeTail = (int*)((char*)newNode + newSize + 4);

            *newNode = newSize;
            *newNodeTail = newSize;


        }

        //merge left-only
        else if(leftGood && !rightGood) {

            int* newNode = (int*)((char*)leftSize - *leftSize - 4);

            int newSize = *mySize + *leftSize + 8;

            int* newNodeTail = (int*)((char*)newNode + newSize + 4);

            *newNode = newSize;
            *newNodeTail = newSize;


        }

        //merge right-only
        else if(!leftGood && rightGood) {

            int* newNodeTail = (int*)((char*)rightSize + *rightSize + 4);

            int newSize = *mySize + *rightSize + 8;


            *mySize = newSize;
            *newNodeTail = newSize;


        }

        //no merging
        else {

            int* foot = (int*)(me + *mySize + 4);

            *foot = *foot * -1;

        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }
};

#endif // Allocator_h
